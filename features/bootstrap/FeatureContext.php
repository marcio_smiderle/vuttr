<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;
use Behat\Testwork\Hook\Scope\AfterSuiteScope;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawMinkContext implements Context
{
    private $kernel;
    private $entityManager;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->entityManager = $kernel->getContainer()
                             ->get('doctrine')->getManager();
        //$this->createDb();
    }

    public function createDb()
    {
        $schemaTool = new \Doctrine\ORM\Tools\SchemaTool($this->entityManager);
        $classes = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $schemaTool->dropSchema($classes);
        $schemaTool->createSchema($classes);
        //print_r($this->entityManager->getConnection()->getParams());
    }

    public function clearData()
    {
        $purger = new ORMPurger($this->entityManager);
        $purger->purge();
    }

    /** @BeforeSuite */
    public static function setup(BeforeSuiteScope $scope)
    {
        exec('bin/console doctrine:database:drop --force');
        exec('bin/console doctrine:database:create');
        if (strpos(getenv('DATABASE_URL'), 'pgsql') === 0) {
            exec('bin/console doctrine:query:sql "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\""');
        }
        exec('bin/console doctrine:schema:create');
    }

    /** @AfterSuite */
    public static function teardown(AfterSuiteScope $scope)
    {
    }

    /**
     * @Given the tool repository is empty
     */
    public function theToolRepositoryIsEmpty()
    {
        $this->clearData();
    }

    /**
     * @When I list the tools
     */
    public function iListTheTools()
    {
        throw new PendingException();
    }

    /**
     * @Then I get an empty list
     */
    public function iGetAnEmptyList()
    {
        throw new PendingException();
    }

    /**
     * @Given there is some tool registered
     */
    public function thereIsSomeToolRegistered()
    {
        throw new PendingException();
    }

    /**
     * @Then I get a populated list
     */
    public function iGetAPopulatedList()
    {
        throw new PendingException();
    }

    /**
     * @When I list the tools with a :arg1 filter
     */
    public function iListTheToolsWithAFilter($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then I get a list with items with :arg1 in the filter
     */
    public function iGetAListWithItemsWithInTheFilter($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When I edit a tool
     */
    public function iEditATool()
    {
        throw new PendingException();
    }

    /**
     * @Then I get a modified tool
     */
    public function iGetAModifiedTool()
    {
        throw new PendingException();
    }

    /**
     * @When I show a tool
     */
    public function iShowATool()
    {
        throw new PendingException();
    }

    /**
     * @Then I get a detailed view of a tool
     */
    public function iGetADetailedViewOfATool()
    {
        throw new PendingException();
    }

    /**
     * @When I delete a tool
     */
    public function iDeleteATool()
    {
        throw new PendingException();
    }

    /**
     * @Then the tool is removed from repository
     */
    public function theToolIsRemovedFromRepository()
    {
        throw new PendingException();
    }

}
