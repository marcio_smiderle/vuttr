<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behatch\HttpCall\Request;
use Behatch\Context\RestContext;
use Behatch\Json\Json;
use Behatch\Json\JsonInspector;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class RestFeatureContext extends RestContext
{
    protected $inspector;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->inspector = new JsonInspector('javascript');
    }

    /**
     * @When I send a :method request to :url at position :index with body:
     */
    public function iSendARequestToAtPositionWithBody($method, $url, $index, PyStringNode $body)
    {
        $this->request->send('GET', $this->locatePath($url));
        $json = new Json($this->request->getContent());
        $actual = $this->inspector->evaluate($json, 'root');
        $uuid = $actual[$index - 1]->id;
        $urlWithId = $url. '/'. $uuid;

        $this->request->setHttpHeader('Content-Type', 'application/json');
        $this->request->setHttpHeader('Accept', 'application/json');
        return $this->request->send(
            $method,
            $this->locatePath($urlWithId),
            [],
            [],
            $body->getRaw()
        );
    }

    /**
     * @When I send a :method request to :url at position :index
     */
    public function iSendARequestToAtPosition($method, $url, $index)
    {
        $this->request->send('GET', $this->locatePath($url));
        $json = new Json($this->request->getContent());
        $actual = $this->inspector->evaluate($json, 'root');
        $uuid = $actual[$index - 1]->id;
        $urlWithId = $url. '/'. $uuid;

        $this->request->setHttpHeader('Content-Type', 'application/json');
        $this->request->setHttpHeader('Accept', 'application/json');
        return $this->request->send(
            $method,
            $this->locatePath($urlWithId),
            [],
            [],
            null
        );
    }
}
