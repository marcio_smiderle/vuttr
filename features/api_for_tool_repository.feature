Feature: API for Online Tools repository
  In order to query tools
  As an user
  I need to be able to register tools

  Rules:
  - API should respond in port 3000
  - Tool title and address should be unique
  - Should be possible to filter tools by tags

  Scenario: Querying tools before registering
    When I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/tools"
    Then the JSON node "root" should have 0 elements

  Scenario Outline: Registering a Tool

    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    When I send a "POST" request to "/tools" with body:
    """
    {
    "title": "<title>",
    "link": "<link>",
    "description": "<description>",
    "tags": [<tags>]
    }
    """
    Then the response status code should be 201
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json; charset=utf-8"

    Examples:
      | title            | link                              | description                                        | tags                                                                            |
      | Run php versions | https://3v4l.org/                 | Run PHP in 200+ PHP VMs                            | "php", "versions", "php", "teste", "executar"                                   |
      | abc              | http://www.ocr.com                | fffff                                              | "amnb", "ghjd"                                                                  |
      | Google Drive     | https://drive.google.com          | Save docs online                                   | "files", "online"                                                               |
      | hotel            | https://github.com/typicode/hotel | Local app manager, app starter and developer tool. | "node", "organizing", "webapps", "domain", "developer", "https", "proxy", "ssh" |
      | Tidy             | https://htmlformatter.com/        | Free online tool to format an ugly HTML code.      | "html", "format", "validation"                                                     |


  Scenario: Querying tools after registering
    When I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/tools"
    Then the JSON node "root" should have 5 elements
    When I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/tools?tags=files"
    Then the JSON node "root" should have 1 elements

  Scenario: Trying to register tools with wrong data

    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "POST" request to "/tools" with body:
    """
    {
    "title": "Run php versions",
    "link": "https://3v4l.org/",
    "description": "Run PHP in 200+ PHP VMs",
    "tags": ["php", "versions", "test", "executar"]
    }
    """
    Then the response status code should be 400
    And the response should be in JSON

    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "POST" request to "/tools" with body:
    """
    {
    "title": "abc",
    "link": "http://www.ocr.com",
    "tags": ["amnb", "ghjd"]
    }
    """
    Then the response status code should be 400
    And the response should be in JSON

  Scenario: Modifying tools
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "PUT" request to "/tools" at position 1 with body:
    """
    {
    "tags": ["php", "versions", "teste", "executar", "hahaha!"]
    }
    """
    Then the response status code should be 200
    And the response should be in JSON

    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "DELETE" request to "/tools" at position 1
    Then the response status code should be 204

    When I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/tools"
    Then the JSON node "root" should have 4 elements
