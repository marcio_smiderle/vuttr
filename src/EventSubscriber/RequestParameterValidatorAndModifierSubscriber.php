<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
//use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
//use InvalidArgumentException;
use ApiPlatform\Core\Exception\FilterValidationException;

class RequestParameterValidatorAndModifierSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        // return the subscribed events, their methods and priorities
        return [
            // KernelEvents::EXCEPTION => [
            //     ['processException', 10],
            //     ['logException', 0],
            //     ['notifyException', -10],
            // ],
            KernelEvents::REQUEST => [
                ['validateRequest', 21]
                /*['changeRequest', 20]*/
            ]
        ];
    }

    public function validateRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if ($request->getMethod() == Request::METHOD_POST or
           $request->getMethod() == Request::METHOD_PUT) {
            // other than get must have some content
            if (empty($request->getContent())) {
                //throw new InvalidArgumentException('Empty POST or PUT request', 400);
                throw new FilterValidationException(['Empty POST or PUT request']);

                /*$response = new Response();
                $response->setContent('Empty POST or PUT request');
                $response->setStatusCode(400);
                return $response;*/
            }
        }
    }

    /*public function changeRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        print_r($request->query);
        if ($request->query->has('tag')) {
            $request->query->set('tags', $request->query->get('tags'));
        }
        }*/

    // public function processException(GetResponseForExceptionEvent $event)
    // {
    //     // ...
    // }

    // public function logException(GetResponseForExceptionEvent $event)
    // {
    //     // ...
    // }

    // public function notifyException(GetResponseForExceptionEvent $event)
    // {
    //     // ...
    // }
}
