<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This is a Tool found in Internet.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 * @ApiFilter(SearchFilter::class, properties={"tags": "partial"})
 * @UniqueEntity(fields={"title"})
 * @UniqueEntity(fields={"link"})
 */
class Tool
{
    /**
     * @var string|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string Name of the tool as found in web page title.
     *
     * @ORM\Column(type="text",unique=true)
     * @Assert\NotNull
     */
    private $title;

    /**
     * @var string Full web address
     *
     * @ORM\Column(type="text",unique=true)
     * @Assert\Url
     * @Assert\NotNull
     */
    private $link;

    /**
     * @var string A description of the item.
     *
     * @ORM\Column(type="text")
     * @ApiProperty(iri="http://schema.org/description")
     * @Assert\NotNull
     */
    private $description;

    /**
     * @var string[] Keywords this Tool is related to.
     *
     * @ORM\Column(type="simple_array")
     * @Assert\NotNull
     * @Assert\Count(
     *      min = 1,
     *      max = 15,
     *      minMessage = "You must specify at least one tag",
     *      maxMessage = "You can  specify at most {{ limit }} tags"
     * )
     */
    private $tags = [];

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function addTag(string $tag): void
    {
        $key = array_search($tag, $this->tags, true);
        if (false === $key) {
            $this->tags[] = $tag;
        }
    }

    public function removeTag(string $tag): void
    {
        $key = array_search($tag, $this->tags, true);
        if (false !== $key) {
            unset($this->tags[$key]);
        }
    }

    /**
     * @return string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}
