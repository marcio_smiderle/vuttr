FORMAT: 1A

# API Documentation

## Group Person

### /people

#### Retrieves the collection of Person resources. [GET /people{?page}]

+ Parameters

    + page

+ Request

    + Headers

            Accept: application/ld+json

    + Body

+ Response 200 (application/ld+json)

    Person collection response

    + Body

            [
              {
                "email": "aute",
                "password": "in consectetur commodo enim",
                "isActive": false
              }
            ]

    + Schema

            {
              "type": "array",
              "items": {
                "type": "object",
                "description": "A person (alive, dead, undead, or fictional).",
                "externalDocs": {
                  "url": "http://schema.org/Person"
                },
                "properties": {
                  "id": {
                    "readOnly": true,
                    "type": "string"
                  },
                  "givenName": {
                    "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                    "type": "string"
                  },
                  "additionalName": {
                    "description": "an additional name for a Person, can be used for a middle name",
                    "type": "string"
                  },
                  "familyName": {
                    "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                    "type": "string"
                  },
                  "birthDate": {
                    "description": "date of birth",
                    "type": "string",
                    "format": "date-time"
                  },
                  "email": {
                    "description": "email address",
                    "type": "string"
                  },
                  "password": {
                    "type": "string"
                  },
                  "isActive": {
                    "type": "boolean"
                  }
                },
                "required": [
                  "email",
                  "password",
                  "isActive"
                ]
              }
            }

#### Creates a Person resource. [POST]

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "email": "aliqua",
              "password": "irure sint",
              "isActive": true,
              "familyName": "laborum aliquip cupidatat",
              "additionalName": "deserunt repr",
              "givenName": "deserunt"
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Response 201 (application/ld+json)

    Person resource created

    + Body

            {
              "email": "officia anim fugiat sint",
              "password": "mollit eu ut sunt",
              "isActive": false
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "email": "dolore irure mollit esse in",
              "password": "laboris Lorem id",
              "isActive": false,
              "additionalName": "Duis"
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Response 400 (application/ld+json)

    Invalid input

    + Body

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "email": "commodo nulla id",
              "password": "Ut in nulla",
              "isActive": true
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Response 404 (application/ld+json)

    Resource not found

    + Body

### /people/{id}

#### Retrieves a Person resource. [GET]

+ Parameters

    + id (required)

+ Request

    + Headers

            Accept: application/ld+json

    + Body

+ Response 200 (application/ld+json)

    Person resource response

    + Body

            {
              "email": "non qui Excepteur",
              "password": "Excepteur veniam cillum aliquip minim",
              "isActive": true,
              "familyName": "fugiat nulla eiusmod irur",
              "givenName": "labore aute",
              "birthDate": "4357-09-17T11:54:45.649Z"
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Request

    + Headers

            Accept: application/ld+json

    + Body

+ Response 404 (application/ld+json)

    Resource not found

    + Body

#### Removes the Person resource. [DELETE]

+ Parameters

    + id (required)

+ Response 204

    Person resource deleted

    + Body

+ Response 404

    Resource not found

    + Body

#### Replaces the Person resource. [PUT]

+ Parameters

    + id (required)

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "email": "sed oc",
              "password": "mollit exerc",
              "isActive": false,
              "additionalName": "et Ut quis cupidatat minim",
              "givenName": "sed commodo",
              "id": "in fugiat Ut quis",
              "birthDate": "2546-08-31T23:50:56.580Z"
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Response 200 (application/ld+json)

    Person resource updated

    + Body

            {
              "email": "tempo",
              "password": "aliqua dolor reprehenderit dolor consectetur",
              "isActive": true,
              "givenName": "inci"
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "email": "sunt cillum id proident in",
              "password": "ex",
              "isActive": true
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Response 400 (application/ld+json)

    Invalid input

    + Body

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "email": "ea",
              "password": "nostrud deserunt id",
              "isActive": false,
              "familyName": "sit aliquip est occaecat",
              "givenName": "ut laborum in aute",
              "id": "enim consectetur in "
            }

    + Schema

            {
              "type": "object",
              "description": "A person (alive, dead, undead, or fictional).",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "givenName": {
                  "description": "Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.",
                  "type": "string"
                },
                "additionalName": {
                  "description": "an additional name for a Person, can be used for a middle name",
                  "type": "string"
                },
                "familyName": {
                  "description": "Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.",
                  "type": "string"
                },
                "birthDate": {
                  "description": "date of birth",
                  "type": "string",
                  "format": "date-time"
                },
                "email": {
                  "description": "email address",
                  "type": "string"
                },
                "password": {
                  "type": "string"
                },
                "isActive": {
                  "type": "boolean"
                }
              },
              "required": [
                "email",
                "password",
                "isActive"
              ]
            }

+ Response 404 (application/ld+json)

    Resource not found

    + Body

## Group Tool

### /tools

#### Retrieves the collection of Tool resources. [GET /tools{?tags,page}]

+ Parameters

    + tags
    
    + page

+ Request

    + Headers

            Accept: application/ld+json

    + Body

+ Response 200 (application/ld+json)

    Tool collection response

    + Body

            [
              {
                "title": "adipisicing consequat officia et",
                "link": "nisi",
                "description": "laboris dolore",
                "tags": [
                  "aute tempor consequat sit sunt",
                  "incididun",
                  "Lorem officia",
                  "qui eu"
                ]
              },
              {
                "title": "laborum fugiat",
                "link": "labore adipisicing Excepteur mollit sunt",
                "description": "sed labore aliqua ut",
                "tags": [
                  "in",
                  "anim",
                  "et in aute",
                  "in ea culpa dolore",
                  "est"
                ]
              }
            ]

    + Schema

            {
              "type": "array",
              "items": {
                "type": "object",
                "description": "This is a Tool found in Internet.",
                "externalDocs": {
                  "url": "http://schema.org/Thing"
                },
                "properties": {
                  "id": {
                    "readOnly": true,
                    "type": "string"
                  },
                  "title": {
                    "description": "Name of the tool as found in web page title.",
                    "type": "string"
                  },
                  "link": {
                    "description": "Full web address",
                    "type": "string"
                  },
                  "description": {
                    "description": "A description of the item.",
                    "type": "string"
                  },
                  "tags": {
                    "description": "Keywords this Tool is related to.",
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  }
                },
                "required": [
                  "title",
                  "link",
                  "description",
                  "tags"
                ]
              }
            }

#### Creates a Tool resource. [POST]

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "title": "reprehenderit cillum tempor",
              "link": "dolore",
              "description": "exercitation ipsum dese",
              "tags": [
                "sint Duis reprehenderit",
                "dolor ad Excepteur sed"
              ],
              "id": "consectetur sint"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Response 201 (application/ld+json)

    Tool resource created

    + Body

            {
              "title": "elit",
              "link": "reprehenderit labore laborum ad c",
              "description": "ad non",
              "tags": [
                "occaecat",
                "sit quis aliqua magna incididunt"
              ],
              "id": "dolore id e"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "title": "culpa dese",
              "link": "ullamco",
              "description": "ad veniam",
              "tags": [
                "proident dolore es"
              ],
              "id": "occaecat culpa"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Response 400 (application/ld+json)

    Invalid input

    + Body

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "title": "eiusmod consectetur adipisicing",
              "link": "velit labore adipisicing culpa nulla",
              "description": "Ut et exercitation",
              "tags": [
                "laboris cupidatat laborum adipisicing Duis",
                "adipisicing proident conse",
                "mollit laborum"
              ],
              "id": "u"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Response 404 (application/ld+json)

    Resource not found

    + Body

### /tools/{id}

#### Retrieves a Tool resource. [GET]

+ Parameters

    + id (required)

+ Request

    + Headers

            Accept: application/ld+json

    + Body

+ Response 200 (application/ld+json)

    Tool resource response

    + Body

            {
              "title": "dolor voluptate",
              "link": "magna",
              "description": "consectet",
              "tags": [
                "consequat exercitation aliqua",
                "minim",
                "amet cillum"
              ],
              "id": "qui ut aliquip sed"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Request

    + Headers

            Accept: application/ld+json

    + Body

+ Response 404 (application/ld+json)

    Resource not found

    + Body

#### Removes the Tool resource. [DELETE]

+ Parameters

    + id (required)

+ Response 204

    Tool resource deleted

    + Body

+ Response 404

    Resource not found

    + Body

#### Replaces the Tool resource. [PUT]

+ Parameters

    + id (required)

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "title": "esse in",
              "link": "commodo",
              "description": "ipsum amet exercitation pariatur",
              "tags": [
                "do occaecat dolore qui dolore",
                "adipisicing"
              ],
              "id": "mollit"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Response 200 (application/ld+json)

    Tool resource updated

    + Body

            {
              "title": "ut amet do aliquip cupidatat",
              "link": "ullamco laboris culpa",
              "description": "eu exercitation",
              "tags": [
                "minim sed sunt",
                "ad velit dolore"
              ]
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "title": "fugia",
              "link": "mollit pariatur exercitation fugiat Lorem",
              "description": "sint pariatur incididunt",
              "tags": [
                "Excepteur in aliqua enim",
                "magna tempor quis",
                "adipisicing",
                "dolore"
              ],
              "id": "incididunt enim anim in tempor"
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Response 400 (application/ld+json)

    Invalid input

    + Body

+ Request (application/ld+json)

    + Headers

            Accept: application/ld+json

    + Body

            {
              "title": "aliquip Duis",
              "link": "nostrud quis Ut est",
              "description": "Duis mollit ullamco Lorem",
              "tags": [
                "Excepteur quis",
                "",
                "Ut ex ea",
                "occaecat consequat eiusmod magna",
                "dolore reprehenderit pariatur sunt nisi"
              ]
            }

    + Schema

            {
              "type": "object",
              "description": "This is a Tool found in Internet.",
              "properties": {
                "id": {
                  "readOnly": true,
                  "type": "string"
                },
                "title": {
                  "description": "Name of the tool as found in web page title.",
                  "type": "string"
                },
                "link": {
                  "description": "Full web address",
                  "type": "string"
                },
                "description": {
                  "description": "A description of the item.",
                  "type": "string"
                },
                "tags": {
                  "description": "Keywords this Tool is related to.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              },
              "required": [
                "title",
                "link",
                "description",
                "tags"
              ]
            }

+ Response 404 (application/ld+json)

    Resource not found

    + Body


