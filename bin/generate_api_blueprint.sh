#!/bin/sh

bin/console api:openapi:export > var/api_swagger.json && swagger2blueprint var/api_swagger.json > apiary.apilib && rm -f var/api_swagger.json
